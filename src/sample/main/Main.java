package sample.main;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import sample.raid.*;

import java.util.Random;

public class Main extends Application {

    private Stage window;

    private Scene main, chooseInputType, chooseFileNames;

    private ChoiceBox<String> chooseDiskNumber = new ChoiceBox<>();

    private Label title = new Label("Tablice dysków nadmiarowych RAID 3");
    private Label inputData = new Label("Dane wejściowe");
    private Label outputData = new Label("Dane wyjściowe po operacji MERGE");
    private Label singleData = new Label("Dane podzielone na macierze RAID");
    private Label parityBit = new Label("Bity parzystości");
    private Label errorMsg = new Label("");
    private Label dataOperations = new Label("Operacje na danych");

    private Button changeRandomBit = new Button("Zamień 1 losowy bit");
    private Button reverseArr = new Button("Odwróć bity losowego dysku");
    private Button generateReportToFile = new Button("Generuj raport do pliku");
    private Button generateRandom = new Button("Wypełnij losowymi");
    private Button check = new Button("Sprawdź poprawność");
    private Button splitData = new Button("Rozdziel dane na dyski");
    private Button clear = new Button("Wyczysc dane");
    private Button file = new Button("Odtwórz dane z plików");
    private Button keyboard = new Button("Dane z klawiatury");
    private Button merge = new Button("Scal dyski");
    private Button export = new Button("Eksportuj dane do plików");

    private TextArea input = new TextArea("");
    private TextArea outputMerged = new TextArea("");
    private TextArea matrixOne = new TextArea("");
    private TextArea matrixTwo = new TextArea("");
    private TextArea parityBits = new TextArea("");
    private TextArea additionalDisc = new TextArea("");

    private Text chooseInputTypeText = new Text("Wybierz parametry startowe");

    private Text file3Text = new Text("Nazwa pliku nr 3");
    private TextField file3Name = new TextField();

    boolean  anotherDisc = false, inputFromFile;

    private int windowWidth = 1200;
    private int windowHeight = 500;

    private Boolean checkInputData(){
        for(int i = 0;i < input.getText().length(); i++){
            if(input.getText().charAt(i) != '0' && input.getText().charAt(i) != '1')
                return false;
        }
        return true;
    }

    private void chooseFileNamesInitializer(){
        Text file1Text = new Text("Nazwa pliku nr 1");
        Text file2Text = new Text("Nazwa pliku nr 2");
        Text parityBitsText = new Text("Nazwa pliku z bitami parzystości");
        TextField file1Name = new TextField();
        TextField file2Name = new TextField();
        TextField parityBitsName = new TextField();

        Button accept = new Button("Kontynuuj");

        accept.setOnAction(e -> {
            if(!file1Name.getText().isEmpty() && !file2Name.getText().isEmpty() && !parityBitsName.getText().isEmpty()) {
                FileOpener fileOpener = new FileOpener();

                if(!anotherDisc) {
                    fileOpener.readData(file1Name.getText(), file2Name.getText(), parityBitsName.getText());

                    matrixOne.setText(fileOpener.getDataOne());
                    matrixTwo.setText(fileOpener.getDataTwo());
                    parityBits.setText(fileOpener.getParityBits());
                }else if(anotherDisc && !file3Name.getText().isEmpty()){
                    fileOpener.readDataWithAdditionalDisc(file1Name.getText(), file2Name.getText(), file3Name.getText(), parityBitsName.getText());

                    matrixOne.setText(fileOpener.getDataOne());
                    matrixTwo.setText(fileOpener.getDataTwo());
                    additionalDisc.setText(fileOpener.getDataThree());
                    parityBits.setText(fileOpener.getParityBits());
                }

                window.setScene(main);
            }
        });

        file1Name.setMaxWidth(300);
        file2Name.setMaxWidth(300);
        parityBitsName.setMaxWidth(300);
        file1Text.setFont(Font.font("Arial", 18));
        file2Text.setFont(Font.font("Arial", 18));
        parityBitsText.setFont(Font.font("Arial", 18));

        accept.setPrefWidth(300);


        file3Name.setMaxWidth(300);
        file3Text.setFont(Font.font("Arial", 18));
        file3Name.setVisible(false);
        file3Text.setVisible(false);

        file1Name.setText("dysk1.txt");
        file2Name.setText("dysk2.txt");
        file3Name.setText("dysk3.txt");
        parityBitsName.setText("sumakontrolna.txt");

        VBox chooseFileNamesLayout = new VBox(30);
        chooseFileNamesLayout.getChildren().addAll(file1Text, file1Name,file2Text, file2Name, file3Text, file3Name,
                parityBitsText, parityBitsName, accept);
        chooseFileNamesLayout.setPadding(new Insets(20, 20, 20, 450));
        chooseFileNames = new Scene(chooseFileNamesLayout, windowWidth, windowHeight);
    }

    private void chooseInputTypeInitializer(){
        Text keyboardDescription = new Text("Wprowadź dane ręcznie a następnie \n podziel na dyski i wyeksportuj do plików");
        Text fileDescription = new Text("Zaimportuj dane z plików a następnie \n sprawdź ich poprawność");
        chooseDiskNumber.getItems().addAll("2 dyski", "3 dyski");
        chooseDiskNumber.setValue("2 dyski");

        file.setPrefWidth(200);
        keyboard.setPrefWidth(200);
        chooseDiskNumber.setPrefWidth(200);

        chooseInputTypeText.setFont(Font.font("Arial", 16));

        file.setOnAction(e -> {
            if(chooseDiskNumber.getValue().equals("2 dyski"))
                anotherDisc = false;
            else {
                anotherDisc = true;
                additionalDisc.setVisible(true);
                file3Name.setVisible(true);
                file3Text.setVisible(true);
            }

            inputFromFile = true;
            generateRandom.setVisible(false);
            splitData.setVisible(false);
            input.setVisible(false);
            inputData.setVisible(false);
            window.setScene(chooseFileNames);
        });

        keyboard.setOnAction(e -> {
            if(chooseDiskNumber.getValue().equals("2 dyski"))
                anotherDisc = false;
            else {
                anotherDisc = true;
                additionalDisc.setVisible(true);
            }

            inputFromFile = false;
            input.setEditable(true);
            window.setScene(main);
        });

        VBox chooseInputTypeLayout = new VBox(10);
        chooseInputTypeLayout.setPadding(new Insets(150, 20, 20, 500));
        chooseInputTypeLayout.getChildren().addAll(chooseInputTypeText, chooseDiskNumber, file, fileDescription, keyboard, keyboardDescription);
        chooseInputType = new Scene(chooseInputTypeLayout, windowWidth, windowHeight);
    }

    private void mainInitializer(){
        FileOpener fileOpener = new FileOpener();


        generateReportToFile.setOnAction(e ->{
                    if(!anotherDisc) {
                        fileOpener.generateResult(new DiskSet3(matrixOne.getText(), matrixTwo.getText(), parityBits.getText()), errorMsg.getText());
                    }
                    else{
                        fileOpener.generateResult(new DiskSet4(matrixOne.getText(), matrixTwo.getText(), additionalDisc.getText(), parityBits.getText()), errorMsg.getText());
                    }
        });

        check.setOnAction(e -> {
            if(!outputMerged.getText().isEmpty() && !parityBits.getText().isEmpty())
                errorMsg.setText(DataHelpers.check(outputMerged.getText(), parityBits.getText()));
            else
                errorMsg.setText("Brak danych aby sprawdzić poprawność");
        });

        changeRandomBit.setOnAction(e ->{
            if(!matrixOne.getText().isEmpty())
                matrixOne.setText(DataHelpers.changeRandomBit(matrixOne.getText()));
            else
                matrixOne.setText("Brak danych do zmiany");
        });

        reverseArr.setOnAction(e -> {
            Random gen = new Random();

            if (!anotherDisc) {
                    int i = gen.nextInt(2) + 1;
                    System.out.println(i);
                    if (i == 1 && !matrixOne.getText().isEmpty())
                        matrixOne.setText(DataHelpers.rotateArr(matrixOne.getText()));
                    else if (i == 2 &&  !matrixTwo.getText().isEmpty())
                        matrixTwo.setText(DataHelpers.rotateArr(matrixTwo.getText()));
                    else if (i == 1 &&  matrixOne.getText().isEmpty()) {
                        matrixOne.setText("Brak danych w dysku nr.1");
                    } else if (i == 2 &&  matrixTwo.getText().isEmpty()) {
                        matrixTwo.setText("Brak danych w dysku nr.2");
                    }
                }
                else{
                    int i = gen.nextInt(3) + 1;
                    System.out.println(i);
                    if (i == 1 && !matrixOne.getText().isEmpty())
                        matrixOne.setText(DataHelpers.rotateArr(matrixOne.getText()));
                    else if (i == 2 && !matrixTwo.getText().isEmpty())
                        matrixTwo.setText(DataHelpers.rotateArr(matrixTwo.getText()));
                    else if (i == 3 && !additionalDisc.getText().isEmpty())
                        additionalDisc.setText(DataHelpers.rotateArr(additionalDisc.getText()));
                    else if (i == 1 && matrixOne.getText().isEmpty()){
                        matrixOne.setText("Brak danych w dysku nr.1");
                    } else if (i == 2 && matrixTwo.getText().isEmpty()) {
                        matrixTwo.setText("Brak danych w dysku nr.2");
                    } else if (i == 3 && additionalDisc.getText().isEmpty()){
                        additionalDisc.setText("Brak danych na dodatkowym dysku nr.3");
                    }
                }
        });


        generateRandom.setOnAction(e -> {
            Random random = new Random();
            String out = "";
            if(anotherDisc)
                for(int i = 0;  i < 24; i++)
                    out += random.nextInt(2);
            else
                for(int i = 0; i < 16; i++)
                    out += random.nextInt(2);

            input.setText(out);
            input.setEditable(false);
        });


        clear.setOnAction(e ->{
                    input.setText("");
                    matrixOne.setText("");
                    matrixTwo.setText("");
                    outputMerged.setText("");
                    parityBits.setText("");
                    input.setEditable(true);
             if(anotherDisc)
                    additionalDisc.setText("");
        });

        splitData.setOnAction(e -> {
            String inputString = input.getText();
            if(checkInputData() && ((anotherDisc && inputString.length() == 24) || (!anotherDisc && inputString.length() == 16))){
                if(!anotherDisc) {
                    DiskSet3 diskSet3 = DataHelpers.splitInto3Disks(inputString);
                    matrixOne.setText(diskSet3.getDisc1());
                    matrixTwo.setText(diskSet3.getDisc2());
                    parityBits.setText(diskSet3.getDiscParity());
                }else {
                    DiskSet4 diskSet4 =  DataHelpers.splitInto4Disks(inputString);
                    matrixOne.setText(diskSet4.getDisc1());
                    matrixTwo.setText(diskSet4.getDisc2());
                    additionalDisc.setText(diskSet4.getDisc3());
                    parityBits.setText(diskSet4.getDiscParity());
                }
                outputMerged.setText("");
                input.setEditable(false);
                window.setScene(main);
            }else{
                outputMerged.setText("Niepoprawne dane wejściowe");
                window.setScene(main);
            }
        });

        merge.setOnAction(e -> {
            if(!anotherDisc)
                outputMerged.setText(DataHelpers.mergeData(matrixOne.getText(), matrixTwo.getText()));
            else
                outputMerged.setText(DataHelpers.mergeData(matrixOne.getText(), matrixTwo.getText(), additionalDisc.getText()));
        });

        export.setOnAction(e -> {
            if(!inputFromFile){
                if(!anotherDisc)
                    fileOpener.save(new DiskSet3(matrixOne.getText(), matrixTwo.getText(), parityBits.getText()));
                else
                    fileOpener.save(new DiskSet4(matrixOne.getText(), matrixTwo.getText(), additionalDisc.getText(), parityBits.getText()));
            }
        });


        title.setAlignment(Pos.CENTER_RIGHT);
        title.setFont(Font.font("Arial",28));
        title.setPadding(new Insets(0,0,20,0));


        input.setEditable(false);
        input.setPrefRowCount(2);
        outputMerged.setEditable(false);
        outputMerged.setPrefRowCount(2);
        matrixOne.setEditable(false);
        matrixOne.setPrefRowCount(1);
        matrixTwo.setEditable(false);
        matrixTwo.setPrefRowCount(1);
        parityBits.setEditable(false);
        parityBits.setPrefRowCount(1);
        additionalDisc.setEditable(false);
        additionalDisc.setPrefRowCount(1);
        additionalDisc.setVisible(false);

        changeRandomBit.setPrefWidth(400);
        changeRandomBit.setPrefHeight(30);
        reverseArr.setPrefWidth(400);
        reverseArr.setPrefHeight(30);
        generateReportToFile.setPrefWidth(400);
        generateReportToFile.setPrefHeight(30);
        generateRandom.setPrefWidth(400);
        generateRandom.setPrefHeight(30);
        splitData.setPrefWidth(400);
        splitData.setPrefHeight(30);
        clear.setPrefWidth(400);
        clear.setPrefHeight(30);
        check.setPrefWidth(400);
        check.setPrefHeight(30);
        merge.setPrefWidth(400);
        merge.setPrefHeight(30);
        export.setPrefWidth(400);
        export.setPrefHeight(30);

        changeRandomBit.setFont(Font.font("Arial",15));
        reverseArr.setFont(Font.font("Arial",15));
        generateReportToFile.setFont(Font.font("Arial",15));
        generateRandom.setFont(Font.font("Arial",15));
        splitData.setFont(Font.font("Arial",15));
        clear.setFont(Font.font("Arial",15));
        check.setFont(Font.font("Arial", 15));
        merge.setFont(Font.font("Arial", 15));
        dataOperations.setFont(Font.font("Arial", 15));
        export.setFont(Font.font("Arial", 15));

        GridPane raid = new GridPane();
        raid.setAlignment(Pos.TOP_LEFT);
        raid.setHgap(10);
        raid.setVgap(10);
        raid.setPadding(new Insets(10,10,10,10));
        raid.add(title,1,1, 3, 1);
        raid.add(inputData,1,2);
        raid.add(input,1,3);
        raid.add(generateRandom,2,3);
        raid.add(splitData,1,4);
        raid.add(singleData,1,6);
        raid.add(matrixOne,1,7);
        raid.add(matrixTwo,2,7);
        raid.add(parityBit,4,6);
        raid.add(parityBits,4,7);
        raid.add(additionalDisc,3,7);
        raid.add(dataOperations, 1, 8);
        raid.add(changeRandomBit,1,9);
        raid.add(reverseArr,2,9);
        raid.add(merge,1,11);
        raid.add(check,2,11);
        raid.add(outputData,1,12);
        raid.add(outputMerged,1,13);
        raid.add(clear,2,13);
        raid.add(generateReportToFile, 3, 13);
        raid.add(export, 4, 13);
        raid.add(errorMsg,1,14);


        main = new Scene(raid,windowWidth,windowHeight);
    }

    @Override
    public void start(Stage primaryStage){
        window = primaryStage;
        window.setTitle("RAID 3");

        chooseInputTypeInitializer();
        chooseFileNamesInitializer();
        mainInitializer();

        window.setScene(chooseInputType);
        window.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
