package sample.raid;

import java.util.Random;

public class DataHelpers {

    public static char xor(char a, char b) {
        if (a == b)
            return '0';
        else
            return '1';
    }


    public static char xor(char a, char b, char c) {
        return xor(xor(a,b), c);
    }

    public static String changeRandomBit(String input) {
        String output = "";
        Random gen = new Random();

        if((input==null || input.isEmpty())){
            output += "Uzupełnij dane wejściowe";
            return "";
        }
        else {
            char[] arr = input.toCharArray();
            int val = gen.nextInt(arr.length);
            if (arr[val] == '1')
                arr[val] = '0';
            else
                arr[val] = '1';

            return String.valueOf(arr);
        }
    }

    public static String rotateArr(String input) {
        String output = "";
        char[] arr = input.toCharArray();

        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == '1')
                output += '0';
            else if (arr[i] == '0')
                output += '1';
        }
        return output;
    }

    public static String  check(String output, String bits) {

        boolean flag = false;

        String damagedBit = "";

        String err = "";

        if(output.length() == 16)
            for (int i = 0; i < bits.length(); i++) {
                if (xor(output.charAt(i), output.charAt(i+8)) != bits.charAt(i)) {
                    damagedBit += (i) + " ";
                    flag = true;
                }
            }
        else if(output.length() == 24)
            for (int i = 0; i < bits.length(); i++) {
                if (xor(output.charAt(i), output.charAt(i+8), output.charAt(i+16)) != bits.charAt(i)) {
                    damagedBit += (i) + " ";
                    flag = true;
                }
            }

        if (!flag)
            err = "Brak uszkodzonych bitów";
        else
            err = "Uszkodzone bity na pozycjach: " + damagedBit;

        return err;
    }

    public static String mergeData(String dataOne, String dataTwo) {
        char[] valuesOne = dataOne.toCharArray();
        char[] valuesTwo = dataTwo.toCharArray();

        String dataOutput = "";

        for (int i = 0; i < valuesOne.length/8; i++) {
            for(int j = 0; j < 8; j++)
                dataOutput += valuesOne[i+j];
            for(int j = 0; j < 8; j++)
                dataOutput += valuesTwo[i+j];
        }
        return dataOutput;

    }

    public static String mergeData(String dataOne, String dataTwo, String dataThree) {
        char[] valuesOne = dataOne.toCharArray();
        char[] valuesTwo = dataTwo.toCharArray();
        char[] valuesThree = dataThree.toCharArray();

        String dataOutput = "";

        for (int i = 0; i < valuesOne.length/8; i++) {
            for(int j = 0; j < 8; j++)
                dataOutput += valuesOne[i+j];
            for(int j = 0; j < 8; j++)
                dataOutput += valuesTwo[i+j];
            for(int j = 0; j < 8; j++)
                dataOutput += valuesThree[i+j];
        }
        return dataOutput;

    }

    public static DiskSet3 splitInto3Disks(String input) {
        StringBuffer disc1 = new StringBuffer();
        StringBuffer disc2 = new StringBuffer();
        StringBuffer discParity = new StringBuffer();

        for (int i = 0; i < input.length() / 16; i++) {
            String disc1Byte = input.substring(16 * i + 0, 16 * i + 8);
            String disc2Byte = input.substring(16 * i + 8, 16 * i + 16);
            StringBuffer parityByte = new StringBuffer();
            for (int j = 0; j < 8; j++) {
                parityByte.append(xor(disc1Byte.charAt(j), disc2Byte.charAt(j)));
            }
            disc1.append(disc1Byte);
            disc2.append(disc2Byte);
            discParity.append(parityByte);
        }

        return new DiskSet3(disc1.toString(), disc2.toString(), discParity.toString());
    }

    public static DiskSet4 splitInto4Disks(String input) {
        StringBuffer disc1 = new StringBuffer();
        StringBuffer disc2 = new StringBuffer();
        StringBuffer disc3 = new StringBuffer();
        StringBuffer discParity = new StringBuffer();

        for (int i = 0; i < input.length() / 24; i++) {
            String disc1Byte = input.substring(24 * i + 0, 24 * i + 8);
            String disc2Byte = input.substring(24 * i + 8, 24 * i + 16);
            String disc3Byte = input.substring(24 * i + 16, 24 * i + 24);
            StringBuffer parityByte = new StringBuffer();
            for (int j = 0; j < 8; j++) {
                parityByte.append(xor(disc1Byte.charAt(j), disc2Byte.charAt(j), disc3Byte.charAt(j)));
            }
            disc1.append(disc1Byte);
            disc2.append(disc2Byte);
            disc3.append(disc3Byte);
            discParity.append(parityByte);
        }

        return new DiskSet4(disc1.toString(), disc2.toString(), disc3.toString(), discParity.toString());
    }
}
