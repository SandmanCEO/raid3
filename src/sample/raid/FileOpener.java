package sample.raid;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

public class FileOpener {

    private String dataOne;
    private String dataTwo;
    private String dataThree;
    private String parityBits;

    public void readData(String fileOne, String fileTwo, String fileParity) {

        if (!Files.exists(Paths.get(fileOne))) {
            System.out.println("Nie można odnaleźć pliku: " + fileOne);
            System.exit(0);
        }
        if (!Files.exists(Paths.get(fileTwo))) {
            System.out.println("Nie można odnaleźć pliku: " + fileTwo);
            System.exit(0);
        }
        if (!Files.exists(Paths.get(fileParity))) {
            System.out.println("Nie można odnaleźć pliku: " + fileParity);
            System.exit(0);
        }

        try {
            BufferedReader bits = new BufferedReader(new FileReader(fileParity));
            System.out.println("Otworzono plik z bitami");
            BufferedReader inDataOne = new BufferedReader(new FileReader(fileOne));
            System.out.println("Otworzono pierwszy plik z danymi");
            BufferedReader inDataTwo = new BufferedReader(new FileReader(fileTwo));
            System.out.println("Otworzono drugi plik z danymi");

            dataOne = inDataOne.readLine();
            dataTwo = inDataTwo.readLine();
            parityBits = bits.readLine();

            bits.close();
            inDataOne.close();
            inDataTwo.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void readDataWithAdditionalDisc(String file1, String file2, String file3, String parityName) {

        if (!Files.exists(Paths.get(file1))) {
            System.out.println("Nie można odnaleźć pliku: " + file1);
            System.exit(0);
        }
        if (!Files.exists(Paths.get(file2))) {
            System.out.println("Nie można odnaleźć pliku: " + file2);
            System.exit(0);
        }
        if (!Files.exists(Paths.get(file3))) {
            System.out.println("Nie można odnaleźć pliku: " + file3);
            System.exit(0);
        }
        if (!Files.exists(Paths.get(parityName))) {
            System.out.println("Nie można odnaleźć pliku: " + parityName);
            System.exit(0);
        }

        try {
            BufferedReader bits = new BufferedReader(new FileReader(parityName));
            System.out.println("Otworzono plik z bitami");
            BufferedReader inDataOne = new BufferedReader(new FileReader(file1));
            System.out.println("Otworzono pierwszy plik z danymi");
            BufferedReader inDataTwo = new BufferedReader(new FileReader(file2));
            System.out.println("Otworzono drugi plik z danymi");
            BufferedReader inDataThree = new BufferedReader(new FileReader(file3));
            System.out.println("Otworzono trzeci plik z danymi");

            dataOne = inDataOne.readLine();
            dataTwo = inDataTwo.readLine();
            dataThree = inDataThree.readLine();
            parityBits = bits.readLine();


            bits.close();
            inDataOne.close();
            inDataTwo.close();
            inDataThree.close();


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void save(DiskSet3 diskSet3) {
        String fileOnePath = "DyskPierwszy.txt";
        String fileTwoPath = "DyskDrugi.txt";
        String bitFilePath = "DyskZBitamiParzystosci.txt";

        if (!Files.exists(Paths.get(fileOnePath))) {
            createFile(fileOnePath);
        }
        if (!Files.exists(Paths.get(fileTwoPath))) {
            createFile(fileTwoPath);
        }
        if (!Files.exists(Paths.get(bitFilePath))) {
            createFile(bitFilePath);
        }

        try {
            BufferedWriter outDataOneFile = new BufferedWriter(new FileWriter(fileOnePath));
            BufferedWriter outDataTwoFile = new BufferedWriter(new FileWriter(fileTwoPath));
            BufferedWriter outParityBitFile = new BufferedWriter(new FileWriter(bitFilePath));

            outDataOneFile.write(diskSet3.getDisc1());
            outDataTwoFile.write(diskSet3.getDisc2());
            outParityBitFile.write(diskSet3.getDiscParity());

            outDataOneFile.close();
            outDataTwoFile.close();
            outParityBitFile.close();

            System.out.println("Zapisano plik: " + fileOnePath);
            System.out.println("Zapisano plik: " + fileTwoPath);
            System.out.println("Zapisano plik: " + bitFilePath);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void save(DiskSet4 diskSet4) {
        String fileOnePath = "DyskPierwszy.txt";
        String fileTwoPath = "DyskDrugi.txt";
        String fileThreePath = "DyskTrzeci.txt";
        String bitFilePath = "DyskZBitamiParzystosci.txt";

        if (!Files.exists(Paths.get(fileOnePath))) {
            createFile(fileOnePath);
        }
        if (!Files.exists(Paths.get(fileTwoPath))) {
            createFile(fileTwoPath);
        }
        if (!Files.exists(Paths.get(bitFilePath))) {
            createFile(fileThreePath);
        }
        if (!Files.exists(Paths.get(bitFilePath))) {
            createFile(bitFilePath);
        }

        try {
            BufferedWriter outDataOneFile = new BufferedWriter(new FileWriter(fileOnePath));
            BufferedWriter outDataTwoFile = new BufferedWriter(new FileWriter(fileTwoPath));
            BufferedWriter outDataThreeFile = new BufferedWriter(new FileWriter(fileThreePath));
            BufferedWriter outParityBitFile = new BufferedWriter(new FileWriter(bitFilePath));

            outDataOneFile.write(diskSet4.getDisc1());
            outDataTwoFile.write(diskSet4.getDisc2());
            outDataThreeFile.write(diskSet4.getDisc3());
            outParityBitFile.write(diskSet4.getDiscParity());

            outDataOneFile.close();
            outDataTwoFile.close();
            outDataThreeFile.close();
            outParityBitFile.close();

            System.out.println("Zapisano plik: " + fileOnePath);
            System.out.println("Zapisano plik: " + fileTwoPath);
            System.out.println("Zapisano plik: " + fileThreePath);
            System.out.println("Zapisano plik: " + bitFilePath);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void generateResult(DiskSet3 diskSet3, String errors) {

        String filePath = "raport.txt";

        if (!Files.exists(Paths.get(filePath))) {
            createFile(filePath);
        }

        try {
            BufferedWriter out = new BufferedWriter(new FileWriter(filePath));

            out.write("RAID 3:    Dysk nr.1: " + diskSet3.getDisc1() + "    Dysk nr.2: " + diskSet3.getDisc2()
                    + "    Bity parzystości: " + diskSet3.getDiscParity() + "\r\n");
            out.write("Wyjście:   Dane po połączeniu: " + DataHelpers.mergeData(diskSet3.getDisc1(), diskSet3.getDisc2()) + "\r\n");

            out.write(errors);

            out.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void generateResult(DiskSet4 diskSet4, String errors) {

        String filePath = "raport.txt";

        if (!Files.exists(Paths.get(filePath))) {
            createFile(filePath);
        }

        try {
            BufferedWriter out = new BufferedWriter(new FileWriter(filePath));

            out.write("RAID 3:    Dysk nr 1: " + diskSet4.getDisc1() + "    Dysk nr 2: " + diskSet4.getDisc2() +
                    "       Dysk nr 3:" + diskSet4.getDisc3()
                    + "    Bity parzystości: " + diskSet4.getDiscParity() + "\r\n");
            out.write("Wyjście:   Dane po połączeniu: " + DataHelpers.mergeData(diskSet4.getDisc1(), diskSet4.getDisc2(), diskSet4.getDisc3()) + "\r\n");

            out.write(errors);

            out.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }



    private void createFile(String path) {
        try {
            Files.createFile(Paths.get(path));
            System.out.println("Utworzono plik: " + path);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public String getDataOne() {
        return dataOne;
    }

    public void setDataOne(String dataOne) {
        this.dataOne = dataOne;
    }

    public String getDataTwo() {
        return dataTwo;
    }

    public String getDataThree() {
        return dataThree;
    }

    public void setDataTwo(String dataTwo) {
        this.dataTwo = dataTwo;
    }

    public String getParityBits() {
        return parityBits;
    }

    public void setParityBits(String parityBits) {
        this.parityBits = parityBits;
    }
}
