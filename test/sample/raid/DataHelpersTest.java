package sample.raid;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class DataHelpersTest {

    @Test
    void rotateArrayTest() {
        String input = "1010";
        String expectedOutput = "0101";

        String output = DataHelpers.rotateArr(input);

        Assertions.assertEquals(expectedOutput, output);
    }

    @Test
    void xorTest(){
        Assertions.assertEquals(DataHelpers.xor('1', '0'), '1');
        Assertions.assertEquals(DataHelpers.xor('1', '1'), '0');
        Assertions.assertEquals(DataHelpers.xor('0', '1'), '1');
        Assertions.assertEquals(DataHelpers.xor('0', '0'), '0');
    }

    @Test
    void threeWayXorTest(){
        Assertions.assertEquals(DataHelpers.xor('0', '0', '0'), '0');
        Assertions.assertEquals(DataHelpers.xor('0', '0', '1'), '1');
        Assertions.assertEquals(DataHelpers.xor('0', '1', '0'), '1');
        Assertions.assertEquals(DataHelpers.xor('0', '1', '1'), '0');
        Assertions.assertEquals(DataHelpers.xor('1', '0', '0'), '1');
        Assertions.assertEquals(DataHelpers.xor('1', '0', '1'), '0');
        Assertions.assertEquals(DataHelpers.xor('1', '1', '0'), '0');
        Assertions.assertEquals(DataHelpers.xor('1', '1', '1'), '1');
    }

    @Test
    void changeRandomBitTest(){
        String test = "01100101";

        Assertions.assertNotEquals(DataHelpers.changeRandomBit(test), test);
    }

    @Test
    void checkTest(){
        String input = "1010110001010110";
        String parityBits = "11111010";

        Assertions.assertEquals(DataHelpers.check(input, parityBits), "Brak uszkodzonych bitów");
    }

    @Test
    void mergeTest(){
        String input1 = "00110010";
        String input2 = "01110011";
        String output = "0011001001110011";

        Assertions.assertEquals(DataHelpers.mergeData(input1, input2), output);
    }

    @Test
    void threeWayMergeTest(){
        String input1 = "00010100";
        String input2 = "11110011";
        String input3 = "11111101";
        String output = "000101001111001111111101";

        Assertions.assertEquals(DataHelpers.mergeData(input1, input2, input3), output);
    }

    @Test
    void splitInto3DisksTest(){
        String input = "0100010011011111";
        String disk1 = "01000100";
        String disk2 = "11011111";
        String pairityBits = "10011011";
        DiskSet3 diskSet3 = DataHelpers.splitInto3Disks(input);

        Assertions.assertEquals(diskSet3.getDisc1(), disk1);
        Assertions.assertEquals(diskSet3.getDisc2(), disk2);
        Assertions.assertEquals(diskSet3.getDiscParity(), pairityBits);
    }

    @Test
    void splitInto4DisksTest(){
        String input = "011010000001101000010110";
        String disk1 = "01101000";
        String disk2 = "00011010";
        String disk3 = "00010110";
        String pairityBits = "01100100";
        DiskSet4 diskSet4 = DataHelpers.splitInto4Disks(input);

        Assertions.assertEquals(diskSet4.getDisc1(), disk1);
        Assertions.assertEquals(diskSet4.getDisc2(), disk2);
        Assertions.assertEquals(diskSet4.getDisc3(), disk3);
        Assertions.assertEquals(diskSet4.getDiscParity(), pairityBits);
    }
}